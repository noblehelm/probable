<?php
session_start();
error_reporting(E_ALL);
ini_set("display_errors", 1);
include "model.php";
if(isset($_FILES["userfile"]) && is_uploaded_file($_FILES["userfile"]["tmp_name"])){
  $name = $_FILES["userfile"]["tmp_name"];
  $myfile = fopen("$name", "r") or die("Unable to open file!");
  $content = fread($myfile,filesize("$name"));
  fclose($myfile);
  $probs = new Probability($content);
}
else{
  $probs = new Probability($_GET["textRawSet"]);
}
?>

<!DOCTYPE html>
<html>
  <head>
    <title>Probabilidade e Estatística</title>
    <link rel="stylesheet" href="css/bootstrap.css">
    <script src="js/bootstrap.js"></script>
  </head>
  <body>
    <div class="container">
      <p>
        <label for="showRawSet">Dados brutos</label>
        <div class="well well-sm" id="showRawSet">
          <?php echo $probs->getRaw(); ?>
        </div>
      </p>
      <p>
        <label for="">Rol</label>
        <div class="well well-sm">
          <?php foreach($probs->getRol() as $var){ echo "$var "; } ?>
        </div>
      </p>
      <p>
        <label for=""><i>Range</i></label>
        <div class="well well-sm">
          <?php echo $probs->getRange(); ?>
        </div>
      </p>
      <p>
        <label for="">Distribuição de frequência</label>
        <table class="table">
          <tr>
            <th>
              Xi
            </th>
            <th>
              Fi
            </th>
            <th>
              Fac
            </th>
            <th>
              fi
            </th>
          </tr>

          <?php foreach($probs->getAbsoluteFrequency() as $key => $var){ echo "<tr><td>".$key."</td><td>".$var."</td><td>".$probs->cumulativeAbsoluteFrequency($key)."</td><td>".$probs->relativeFrequency($var)."</td></tr>"; } ?>

        </table>
      </p>
      <p>
        <label for="">Número de classes</label>
        <div class="well well-sm">
          <?php echo $probs->getNumberOfClasses(); ?>
        </div>
      </p>
      <p>
        <label for="">Amplitude das classes</label>
        <div class="well well-sm">
          <?php echo $probs->getClassRange(); ?>
        </div>
      </p>
      <p>
        <label for="">Mediana</label>
        <div class="well well-sm">
          <?php echo $probs->getMedian(); ?>
        </div>
      </p>
      <p>
        <label for="">Moda</label>
        <div class="well well-sm">
          <?php echo $probs->moda(); ?>
        </div>
      </p>
      <p>
        <label for="">Desvio médio</label>
        <table class="table">
          <tr>
            <th>
              Xi
            </th>
            <th>
              DM
            </th>
          </tr>
          <?php foreach($probs->getAbsoluteFrequency() as $key => $var){ echo "<tr><td>".$key."</td><td>".$probs->averageDeviation($key)."</td></tr>"; } ?>
        </table>
      </p>
      <p>
        <label for="">Desvio médio absoluto</label>
        <div class="well well-sm">
          <?php echo $probs->absoluteAverageDeviation(); ?>
        </div>
      </p>
      <p>
        <label for="">Variância média</label>
        <table class="table">
          <tr>
            <th>
              Xi
            </th>
            <th>
              Variância
            </th>
          </tr>
          <?php foreach($probs->getAbsoluteFrequency() as $key => $var){ echo "<tr><td>".$key."</td><td>".$probs->averageVariance($key)."</td></tr>"; } ?>
        </table>
      </p>
      <p>
        <label for="">Variância absoluta</label>
        <div class="well well-sm">
          <?php echo $probs->getAbsoluteVariance(); ?>
        </div>
      </p>
      <p>
        <label for="">Desvio-padrão</label>
        <div class="well well-sm">
          <?php echo $probs->getDefaultDeviation(); ?>
        </div>
      </p>
    </div>
  </body>
</html>
