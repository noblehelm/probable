<?php session_start(); ?>

<!DOCTYPE html>
<html>
<head>
  <title>Probabilidade e Estatística</title>
  <link rel="stylesheet" href="css/bootstrap.css">
  <script src="js/bootstrap.js"></script>
</head>
<body>
  <div class="container">
    <form action="static.php" method="GET">
      <p>
        <div class="form-group">
          <label>Entre com os dados brutos</label>
          <input type="text" class="form-control" name="textRawSet" placeholder="e.g.: 2-5-8-3-1-6-8-3-2-6-8-3-2-6-8"></input>
        </div>
      </p>
      <button type="submit" class="btn btn-default">Enviar</button>
    </form>
    <hr>
    <p>
      <form enctype="multipart/form-data" action="static.php" method="POST">
        <input type="hidden" name="MAX_FILE_SIZE" value="30000" />
        Enviar esse arquivo: <input  name="userfile" type="file" />
        <button type="submit" class="btn btn-default">Enviar arquivo</button>
      </form>
    </p>

  </div>
</body>
</html>
