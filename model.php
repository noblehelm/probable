<?php

class Probability{
  private $TextRaw;
  private $ArrayRaw;
  private $Rol;
  private $SampleSize;
  private $Range;
  private $AbsoluteFrequency;
  private $Median;
  private $Variance;
  private $NumberOfClasses;
  private $ClassRange;

  public function __construct($var){
    $this->TextRaw = $var;
    $this->ArrayRaw = explode("-",$var);
    $this->Rol = $this->ArrayRaw;
    sort($this->Rol);
    $this->SampleSize = count($this->Rol);
    $this->AbsoluteFrequency = array_count_values($this->Rol);
    $this->Range = (end($this->Rol) - reset($this->Rol));
    $this->Median();
    $this->calculateNumberOfClasses();
    $this->calculateClassRange();
  }
  public function calculateClassRange(){
    $this->ClassRange = intval($this->Range/$this->NumberOfClasses);
  }
  public function getClassRange(){
    return $this->ClassRange;
  }
  public function calculateNumberOfClasses(){
    $this->NumberOfClasses = intval(1 + 3.22*(log($this->SampleSize)));
  }
  public function getNumberOfClasses(){
    return $this->NumberOfClasses;
  }
  public function getRaw(){
    return $this->TextRaw;
  }
  public function getRol(){
    return $this->Rol;
  }
  public function getRange(){
    return $this->Range;
  }
  public function getAbsoluteFrequency(){
    return $this->AbsoluteFrequency;
  }
  public function cumulativeAbsoluteFrequency($par){
    $temp = 0;
    foreach($this->AbsoluteFrequency as $key => $value){
      if($key <= $par){
        $temp = $temp + $value;
      }
    }
    return $temp;
  }
  public function relativeFrequency($par){
    $var = count($this->Rol);
    return "$par/$var = ".$par/$var;
  }
  public function arithmeticMean(){
    $temp = 0;
    foreach($this->Rol as $key => $var){
      $temp = $temp + $var;
    }
    return $temp/count($this->Rol);
  }
  public function Median(){
    if(($this->SampleSize%2)==0){
      $TemporaryOne = $this->SampleSize/2;
      $TemporaryTwo = ($this->SampleSize/2)+1;
      $this->Median = intval(($this->ArrayRaw[$TemporaryOne] + $this->ArrayRaw[$TemporaryTwo])/2);
    }
    else{
      $Temporary = ($this->SampleSize+1)/2;
      $this->Median = $this->ArrayRaw[$Temporary];
    }
  }
  public function getMedian(){
    return $this->Median;
  }
  public function moda(){
    $temp = $this->Rol;
    rsort($temp);
    return reset($temp);
  }
  public function averageDeviation($var){
    $temp = $var - $this->Median;
    if($temp < 0){
      $temp = $temp * (-1);
    }
    return $temp;
  }
  public function absoluteAverageDeviation(){
    $populate = 0;
    foreach($this->AbsoluteFrequency as $key => $value){
      $temp = $value - $this->Median;
      if($temp < 0){
        $temp = $temp * (-1);
      }
      $populate = $populate + $temp;
    }
    return $populate/$this->SampleSize;
  }
  public function averageVariance($var){
    return pow($var - $this->Median,2);

  }
  public function absoluteVariance(){
    $temp = 0;
    foreach($this->AbsoluteFrequency as $key => $value){
      $temp = $temp + pow($value - $this->Median,2);
    }
    $this->Variance =  $temp/$this->SampleSize;
  }
  public function getAbsoluteVariance(){
    $this->absoluteVariance();
    return $this->Variance;
  }
  public function getDefaultDeviation(){
    return sqrt($this->Variance);
  }
}
